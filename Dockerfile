# Create a container from Ubuntu.
#FROM python:3
#FROM amancevice/pandas
#FROM alpine:edge
FROM alpine

ARG MAILPASS
EXPOSE 5000

# Credits.
MAINTAINER Kevin Ernst "ernstki@mail.uc.edu"

# Update distro's repositories.
#RUN apt-get update
#RUN apk update

# Install Python (these were for Debian-derived distros)
#RUN apt-get install -y -q build-essential
#RUN apt-get install -y python python-pip curl wget
#RUN apt-get install -y python-dev
#RUN apt-get install -y npm nodejs-legacy

# Install Python and build essentials (Alpine)
RUN apk add --no-cache build-base
# for mysql_config, which one of the dependencies of SQLAlchemy might need
RUN apk add --no-cache mariadb-dev
RUN apk add --no-cache libffi libffi-dev
RUN apk add --no-cache python python-dev py-pip 
# I /think/ this brings in npm
#RUN apk add --no-cache nodejs

# Source: http://blog.zot24.com/tips-tricks-with-alpine-docker/
#RUN apk add --no-cache --repository https://nl.alpinelinux.org/alpine/edge/testing rabbitmq-server

# Install any necessary (global) Python packages
RUN pip install virtualenv

# Install any necessary Node packages
#RUN npm install -g bower

# Create a working directory.
RUN mkdir /deploy

# Create the Supervisor log directory
RUN mkdir /deploy/log

# Add requirements file.
COPY requirements.txt /deploy/
COPY setup.py /deploy/
#COPY supervisord.conf /deploy/

# Copy the Flask app's source and other necessary files
#COPY . /deploy/
COPY bioreactor /deploy/bioreactor
COPY data /deploy/data
COPY docker-launch.sh /
#COPY .bowerrc /deploy/
#COPY bower.json /deploy/

# Set up virtualenv (allowing access to global 'site-packages')
WORKDIR /deploy
RUN virtualenv --system-site-packages venv
RUN venv/bin/pip install -e .

# Run Bower installation
#RUN bower --allow-root install

# Echo the SMTP password into '.mailpass'
RUN echo $MAILPASS > .mailpass

# Source: https://docs.docker.com/engine/reference/builder/#entrypoint
# See also: https://github.com/krallin/tini#using-tini
#ENTRYPOINT ["/sbin/tini", "--"]
CMD /docker-launch.sh
