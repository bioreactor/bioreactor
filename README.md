# ![](img/bioreactor_logo.png)

**Bioreactor**: a Flask+Python scaffolding for web-based bioinformatics
analysis tools

## Quick start

1. Clone this repository to your local machine.
2. If you are not using the [Bioreactor VM][vmrepo], then you're responsible
   for getting a local [RabbitMQ][] server up-and-running.
3. `cd` to where you cloned the repo and create a virtualenv; activate it:

        cd /where/you/cloned/bioreactor
        virtualenv venv
        source venv/bin/activate  # just 'venv/Scripts/activate' on Windows

4. Run the setuptools `setup.py` with pip to install all the dependencies and
   create a `bioreactor` script in your `PATH`, which you can use to perform
   administrative tasks:

        pip install -e .

5. Initalize the database:

        bioreactor initdb

6. Run the Flask application in debug mode:

        FLASK_DEBUG=1
        FLASK_APP=bioreactor/app.py
        bioreactor run --host=0.0.0.0  # or 'flask run', same diff

    You must be in the top-level directory of the cloned repository for this;
    otherwise you'll be treated to a `ImportError: Import by filename is not
    supported.` error.

7. Visit <http://localhost:5000> if you're developing locally or
   <http://localhost:55000> if you're using the VM.

## Excruciating details

### Setting up the development / test server VM

1. Install [Vagrant][]. Packages exists for the commercial OSes and most
   GNU/Linux distros.
2. Clone the "[bioreactor-vm][vmrepo]" repository and run the setup script

        cd ~/path/to/your/dev/stuff
        git clone git@github.uc.edu:Bioreactor/bioreactor-vm.git
        cd bioreactor-vm
        ./setup.sh

More details may be found in the [associated README][vmreadme].

### Establishing a Python "virtual environment"

We're using `virtualenv` to manage the runtime environment of the Bioreactor
application. Basically, virtualenv puts a copy of the Python interpreter and
any libraries installed (_e.g._, with `pip`) within a subdirectory of the
project. This is to reduce the reliance on system packages, and prevent
updates at the OS level from breaking the application.

This directory, conventionally `venv` or `env`, is purposely excluded from
version control in the `.gitignore` for the repository.

In order to set up the virtual environment after having freshly-cloned this
repository, do this (here we are assuming that the repo was cloned to
`/var/www/bioreactor`, which is the setup on the [devlopment VM][vmrepo]):

    # 'cd' to wherever the Bioreactor repository was originally cloned
    cd /var/www/bioreactor

    # 'venv' is conventional; other scripts (mentioned below) rely on it
    virtualenv venv

    # ...actually enter the virtual environment 
    source venv/bin/activate

    # and install necessary packages using 'pip' and the 'requirements.txt' file
    pip install -r requirements.txt

Ordinarily (as described in detail [here][virtualenv]), you would need to
`source venv/bin/activate` in order to enter the virtual environment.  If
you're running on the [Vagrant VM][vmrepo], however, you already have a Bash
alias called `activate` that will work in the same directory where the `venv`
was created.

Your `PS1` prompt string will change to reflect whether or not you're inside
a virtual environment (see the screenshot in the next section). If you wish to
exit the environment `deactivate` is the command to do that. You will still
remain logged in, but packages installed within `venv` won't be available.

### Starting the Bioreactor web application

There are an exasperating number of different methods for launching a Flask
application. These are the two that we use:

```bash
cd /to/where/you/cloned/bioreactor
export FLASK_APP=bioreactor/app.py
export FLASK_DEBUG=1
flask run --host=0.0.0.0 --port=5000

# or
supervisord && supervisorctl status bioreactor
```

The [`supervisord`][supervisor] method has the benefit of starting a Celery
worker for you.  Otherwise, you would want to run a Celery worker in a separate
terminal like this:

```bash
cd /to/where/you/cloned/bioreactor
celery -A bioreactor.tasks worker -E -l INFO
```

Supervisor runs the Flask app a [Gunicorn][] server as a background process. In
this case you'll need to check `log/phyloweb.log` for error messages, as they
won't be echoed to the console as in the other two cases.

The `--host=0.0.0.0` and `--port=5000` arguments to `flask run` are necessary
if you want to be able to access the running Flask application from your host
operating system through the forwarded port (likely 55000) set up by Vagrant
when you installed the ["bioreactor" VM][vmrepo].

On the [VM][vmrepo], however, you _shouldn't_ need to define `FLASK_APP`;
that's done for you by [autoenv][] (see the next section) as soon as you `cd`
into the directory.

## Other tips

### Automatic activation of the virtual environment

Within the [development virtual machine][vmrepo], we have installed a small
helper program called "[autoenv][]", which replaces the `cd` shell builtin
with a shell function that will run arbitrary commands when you change into
a directory.

A common example of "arbitrary commands" is activating a Python virtual
environment when you `cd` into the project directory, as shown in the
screenshot below. _This principle is similar to how some Ruby virtual
environments (e.g., `rbenv`) operate, but not limited to one programming
language or fixed set of commands._

![Example of "autoenv" in action](img/autoenv_example.png)

The actual commands which are run are contained in the [`.env`](.env) file
which is part of the "bioreactor" repository.

For security, because anyone on a shared system could drop a `.env` file in
a writable directory you might happen to traverse, autoenv will prompt you for
confirmation the _first_ time you enter into a directory containing a file
named `.env`, and remember your response for next time.

**NOTE**: There seems to be a bug with the way this first-run prompt works (at
least on the [VM][vmrepo]), in that it appears to hang instead of returning the
shell prompt. Your prompt is still here, just press ENTER one more time and
it'll show up.

You can create a `.env` within any folder on the filesystem of the VM to
achieve the same effect elsewhere.

### Enabling web-based database management

You can use the provided copy of [Adminer][] to manage the database tables for
the Bioreactor application using a simple web interface. On Debian / Ubunutu,
this will require the `php5` and `php5-sqlite` packages be installed (possibly
called, simply `php` and `php-sqlite` in later releases).

You can make the `db` directory web-accessible with an Apache config similar
to what's shown below.  On a "production" system, you also should (at
a minimum) enable HTTP authentication on the `db` subdirectory, because at the
present time Adminer has been modified to load the SQLite database directly
from the disk without prompting for a username or password.

```apache
<Directory /var/www/bioreactor/db>
    AllowOverride all
    Options -Indexes

    # Ref: https://httpd.apache.org/docs/2.4/howto/auth.html
    AuthName "Bioreactor database admin"
    # Create a username/password pair with 'htpasswd -c <dbname> <user>'
    AuthUserFile "/var/www/bioreactor/htpasswd"
    AuthType Basic

    Order deny,allow
    Allow from all
    Require valid-user
</Directory>
Alias "/dbadmin" "/var/www/bioreactor/db"
```

On the development VM, this section is already present in
`/etc/apache2/sites-available/990-bioreactor.conf`, but commented out by
default.

At the present time, Adminer has been hard-coded to open `db/bioreactor.db`
directly off the filesystem, without asking for a username or password.
Support for other database systems may be re-enabled by:

1. having a look at [`db/index.php`](db/index.php) and the
   [Adminer API documentation][adminerapi] and modifying the `login()` method,
   or
2. possibly commenting out the `<FilesMatch>` directive in
   [`db/.htaccess`](db/.htaccess) and loading the `adminer-x.y.z.php` script
   directly in the browser (which is otherwise denied by the `.htaccess`)

[vagrant]:  https://vagrantup.com
[vmreadme]: https://github.uc.edu/Bioreactor/bioreactor-vm/blob/master/README.md
[vmrepo]:   http://github.uc.edu/Bioreactor/bioreactor-vm
[virtualenv]: http://docs.python-guide.org/en/latest/dev/virtualenvs/
[bashcomp]: https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html
[autoenv]: https://github.com/kennethreitz/autoenv 
[adminer]: https://www.adminer.org/
[adminerapi]: https://www.adminer.org/en/extension/
[flaskconfig]: http://flask.pocoo.org/docs/0.11/config/#builtin-configuration-values
[supervisor]: http://supervisord.org/running.html
[gunicorn]: http://docs.gunicorn.org/en/latest/run.html
[rabbitmq]: http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html
