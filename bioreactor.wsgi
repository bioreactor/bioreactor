"""
WSGI connector for Bioreactor

A basic, functional Apache <VirtualHost> configuration stanza for mod_wsgi
would look something like this:

    WSGIDaemonProcess bioreactor threads=5
    WSGIScriptAlias / /var/www/bioreactor/bioreactor.wsgi
    WSGIImportScript /var/www/bioreactor/venv/bin/activate_this.py \
                     process-group=bioreactor application-group=%{GLOBAL}

    <Directory /var/www/bioreactor>
        WSGIProcessGroup bioreactor
        WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
    </Directory>

Helpful References:
- http://flask.pocoo.org/docs/0.11/deploying/mod_wsgi/#configuring-apache
- https://modwsgi.readthedocs.io/en/develop/configuration-directives/WSGIImportScript.html
"""
import sys
path = '/bioreactor'

# ref: https://modwsgi.readthedocs.io/en/develop/user-guides/reloading-source-code.html
if path not in sys.path:
    sys.path.insert(0, path)

import bioreactor
application = bioreactor.create_app()

# vim:ft=python
