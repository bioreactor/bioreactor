from bioreactor import create_app
from bioreactor.helpers.utils import enumerate_files_in

app = create_app()

import bioreactor.cli

if __name__ == '__main__':
    # Tell Werkzeug's run_simple() method to watch the templates directory.
    # I dunno why this isn't automatic; maybe there's some Flask setting for it
    # that I just missed.
    #with app.app_context():
    #    watch_these = enumerate_files_in('templates')

    #app.run(host=app.config['SERVER_HOST'], port=app.config['SERVER_PORT'],
    #        extra_files=watch_these)
    app.run()
