"""
auth imports app and models, but none of those import auth
so we're OK

Ref: http://charlesleifer.com/blog/structuring-flask-apps-a-how-to-for-those-coming-from-django/
"""
from flask_security import Security, SQLAlchemyUserDatastore
from flask_security.utils import encrypt_password

from bioreactor.extensions import db
from bioreactor.models import User, Role

user_datastore = SQLAlchemyUserDatastore(db, User, Role)

# Not sure this is the best place for this
def populate_roles():
    """
    Populate the 'roles' table
    """
    # 'cluster_user's will be able to submit jobs under their own HPC logins
    for role in 'admin', 'user', 'cluster_user':
        user_datastore.find_or_create_role(role)
    db.session.commit()

def create_admin_user(username='admin', password='supersecret'):
    """
    Create a default 'admin' user
    """
    p = encrypt_password(password)
    u = user_datastore.create_user(username=username, password=p)
    r = user_datastore.find_or_create_role('admin')
    user_datastore.add_role_to_user(u, r)
    db.session.commit()
