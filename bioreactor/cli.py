import os
import sys
import re
import click
import sqlalchemy

from flask.cli import FlaskGroup
from flask import current_app
from flask_security.utils import encrypt_password

from bioreactor.extensions import db
from bioreactor.auth import user_datastore, populate_roles, create_admin_user
from bioreactor.models import User, populate_jobs, populate_organisms

# Used in one of the Click commands below to clean up temp files in project
# subdirs
source_dirs = ['.', 'templates']

# Source: http://click.pocoo.org/5/options/#yes-parameters
def abort_if_false(ctx, param, value):
    if not value:
        ctx.abort()

def validate_user(ctx, param, value):
    if re.match('^[a-z][a-z0-9]+$', value):
        return value
    else:
        raise click.BadParameter('illegal characters in username')

@click.group(cls=FlaskGroup)
def cli():
    """
    Management commands for Bioreactor
    """
    pass


@cli.command()
@click.option('--username', help="The user's (alphanumeric) username",
              prompt=True, callback=validate_user,
              default=lambda: os.environ.get('USER', ''))
@click.option('--admin', is_flag=True, help='User is an administrator',
              prompt=True)
#@click.password_option(help="The user's password")
@click.option('--password', prompt=False, hide_input=True,
              confirmation_prompt=True, help="The user's password")
def adduser(username, admin=False, password=None):
    """Creates a new user."""

    click.secho("Creating user '%s'..." % username, fg='yellow')

    if not password:
        import string
        # PEP 506 says this is insecure; 'secrets' is not part of standard library
        # until Python 3.6, though. Ref: https://www.python.org/dev/peps/pep-0506/
        from random import choice
        # source: https://docs.python.org/3.6/library/secrets.html
        with open('/usr/share/dict/words') as f:
            words = [word.strip().lower() for word in f]
            password = ' '.join(choice(words) for i in range(4))

        # If password was left off, echo the generated one to the terminal
        click.secho('Generated password is: ', fg='yellow', nl=False)
        click.echo(password)

    # Use the Flask-Security datastore to create the user
    try:
        p = encrypt_password(password)
        u = user_datastore.create_user(username=username, password=p)
        r = user_datastore.find_or_create_role('user')
        user_datastore.add_role_to_user(u, r)

        if admin:
            r = user_datastore.find_or_create_role('admin')
            user_datastore.add_role_to_user(u, r)

        db.session.commit()
    except sqlalchemy.exc.IntegrityError as e:
        #raise click.BadParameter(
        #        "unable to add user '%s' (already exists?)" % username)
        click.secho("\nUser '%s' already exists. Bailing out." % username,
                    fg='red',err=True)
        sys.exit(1)

    click.secho('Done.', fg='green')

#@cli.command()
#def populate():
#    """Populate 'jobs' table with test data"""
#    click.secho("Populating 'jobs' table with test data... ", fg='yellow',
#                nl=False)
#    db.create_all()
#    populate_jobs_table()
#    click.secho('done.', fg='green')

@cli.command()
@click.option('--yes', is_flag=True, callback=abort_if_false,
              expose_value=False, prompt='Really drop all tables?')
def dropdb():
    """Drop all database tables."""
    db.drop_all()

@cli.command()
@click.option('--yes', is_flag=True, callback=abort_if_false,
              expose_value=False, prompt='Really re-initialize database?')
@click.option('--admin-password', prompt=True, hide_input=True,
              confirmation_prompt=True, help="The admin password")
def initdb(admin_password):
    """Creates tables and default admin user."""

    if current_app.config['SQLALCHEMY_ECHO']:
        current_app.config['SQLALCHEMY_ECHO'] = False

    click.secho('Dropping all tables... ', fg='yellow', nl=False)
    db.drop_all()
    click.secho("done.", fg='green')

    click.secho('Creating table schemata... ', fg='yellow', nl=False)
    db.create_all()
    click.secho("done.", fg='green')

    click.secho("Populating 'roles' table with default roles... ", fg='yellow',
                nl=False)
    populate_roles()
    click.secho("done.", fg='green')

    click.secho("Adding default admin user... ", fg='yellow', nl=False)
    create_admin_user(username='admin', password=admin_password)
    click.secho("done.", fg='green')

    click.secho("Populating 'organism' table with test data... ", fg='yellow',
                nl=False)
    populate_jobs()
    click.secho('done.', fg='green')

    click.secho("Populating 'job' table with test data... ", fg='yellow',
                nl=False)
    populate_organisms()
    click.secho('done.\n', fg='green')

@cli.command()
def clean():
    """
    Clean up .pyc files (and other detritus)
    """
    click.secho('Tidying up the project directories...', fg='yellow')
    os.chdir(current_app.config['PROJECT_ROOT'])
    for subdir in source_dirs:
        click.secho('  - %s' % os.path.abspath(subdir))
        for root, dirs, _ in os.walk(subdir):
            try:
                for f in glob.glob('%s/*.pyc' % root):
                    os.remove(f)
                if '__pycache__' in dirs:
                    click.secho('    * removing %s/__pycache__'
                                % os.path.abspath(subdir))
                    os.system("rm -rf %s/__pycache__" % root)
                    dirs.remove('__pycache__')
            except:
                click.secho('    Oops. A failure occurred cleaning %s' % root,
                            fg='red')
            finally:
                # Don't even bother traversing 'static' dirs:
                if 'static' in dirs:
                    dirs.remove('static')

    click.secho('\nAll done.', fg='green')


if __name__ == '__main__':
    cli()
