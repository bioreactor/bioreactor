import os
import inspect


class BioreactorConfigBadMailPass(Exception):
    pass


class Configuration(object):
    # Source: https://stackoverflow.com/a/31867043
    PACKAGE_DIR = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))
    PROJECT_ROOT = os.path.abspath(os.path.join(PACKAGE_DIR, '..'))

    APP_NAME    = 'Bioreactor'
    ADMIN_EMAIL = 'kevin.ernst@cchmc.org'

    MAIL_USERNAME = 'bioreactor.relay@gmail.com'
    MAIL_SERVER   = 'smtp.gmail.com'
    MAIL_PORT     = 465
    MAIL_USE_SSL  = True
    MAIL_DEBUG    = False
    MAIL_DEFAULT_SENDER = '"Bioreactor" <%s>' % MAIL_USERNAME

    try:
        passfile = os.path.join(PROJECT_ROOT, '.mailpass')
        MAIL_PASSWORD = open(passfile, 'r').read().replace('\n', '')
    except Exception as e:
        MAIL_PASSWORD = os.getenv('BIOREACTOR_MAIL_PASSWORD', '')
        if not MAIL_PASSWORD:
            raise BioreactorConfigBadMailPass(passfile)

    # See http://flask.pocoo.org/docs/0.11/config/
    DEBUG = True
    EXPLAIN_TEMPLATE_LOADING = False
    TEMPLATES_AUTO_RELOAD = True

    ENVIRON     = os.getenv('BIOREACTOR_ENV', 'dev')
    SERVER_HOST = os.getenv('BIOREACTOR_HOST', '0.0.0.0')
    SERVER_PORT = int(os.getenv('BIOREACTOR_PORT', 5000))
    SECRET_KEY  = 'rcXokebHTTbA1tsVhbAXVBvadzPljBIXMdf4vNnPxqTYb7o1PWvey85VFgqSDj5S'

    # FIXME: Authentication should be updated to use Flask-Security (eventually)
    USERNAME    = 'admin'
    PASSWORD    = 'supersecret'

    # Options for the organisms auto-complete REST API
    API_PREFIX  = '/api/v1'

    # Database configuration
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DB_FILENAME = os.path.join(PROJECT_ROOT, 'data/bioreactor.db')
    DB_ORGANISM_CSV = os.path.join(PROJECT_ROOT, 'data/organism_table.csv')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % DB_FILENAME

    # See https://pythonhosted.org/Flask-Security/configuration.html
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_PASSWORD_SALT = 'PinkHimalayanSeaSalt'

    # Celery
    CELERY_BROKER_URL = 'amqp://localhost'
    CELERY_RESULT_BACKEND = 'rpc://'
