from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_restless import APIManager
from flask_security import Security

bs = Bootstrap()
db = SQLAlchemy()
mail = Mail()
apimgr = APIManager()
security = Security()

# I wish I could've implemented this in bioreactor.tasks, but subclassing
# Celery didn't work as I expected, and it looks like you need to set at
# *least* 'broker=' when the thing is instantiated, ruling out a nice clean
# '.init_app()' solution like the other extensions here.
#from bioreactor.config import Configuration as cfg
#taskq = TaskQueue('taskq', broker=cfg.CELERY_BROKER_URL,
#                           backend=cfg.CELERY_RESULT_BACKEND)
