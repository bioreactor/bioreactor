"""
Single entry-point for 'flask run' or WSGI that resolves all the import
dependencies.

Ref: http://charlesleifer.com/blog/structuring-flask-apps-a-how-to-for-those-coming-from-django/
"""
from flask import Flask
from celery import Celery

from bioreactor.config import Configuration as cfg

__all__ = ['create_app', 'create_celery_app']


def create_app(config=None, blueprints=None):
    # my unsophisticated way of dealing with circular imports
    # e.g., factory <-- views, views <-- tasks <-- factory#create_celery_app
    from bioreactor import views
    from bioreactor import models
    from bioreactor.auth import user_datastore
    from bioreactor.extensions import db, bs, mail, apimgr, security #, taskq
    from bioreactor.helpers.jinja import filters as j2_filters
    from bioreactor.helpers.jinja import functions as j2_functions

    # props: https://bitbucket.org/danjac/newsmeme
    EXTENSIONS = [ db, bs, mail ] #, taskq ]

    API_MODELS = [
        (models.Organism, ['GET'], cfg.API_PREFIX),
    ]

    DEFAULT_BLUEPRINTS = [
        (views.frontend, ''),
        (views.account, ''),
        (views.jobs, ''),
        (views.contact, '/contact'),
        (views.api, cfg.API_PREFIX),
    ]

    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    app = Flask(__name__)

    configure_app(app, config)
    configure_template_filters(app, j2_filters)
    configure_template_functions(app, j2_functions)
    configure_blueprints(app, blueprints)
    configure_extensions(app, EXTENSIONS)
    configure_security(app, security, user_datastore)
    # Might be able to combine this with configure_blueprints(), above;
    # see: https://tinyurl.com/kubls6r (Flask-Restless' create_api_blueprint())
    configure_apis(app, apimgr, db, API_MODELS)

    return app


def create_celery_app(app=None):
    """
    Return a Celery instance that has a custom TaskBase wrapped in a Flask
    app context.
    Source(ish): https://github.com/mattupstate/overholt/blob/master/overholt/factory.py
    """
    app = app or create_app()
    celery = Celery(__name__, broker=app.config['CELERY_BROKER_URL'],
                              backend=app.config['CELERY_RESULT_BACKEND'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def configure_app(app, config):

    app.config.from_object(cfg())

    if config is not None:
        app.config.from_object(config)

    app.config.from_envvar('BIOREACTOR_APP_CONFIG', silent=True)


def configure_template_filters(app, filters):

    for filter in filters.__all__:
        app.add_template_filter(getattr(filters, filter))


def configure_template_functions(app, functions):

    for func in functions.__all__:
        app.add_template_global(getattr(functions, func))


def configure_blueprints(app, blueprints):

    for blueprint, url_prefix in blueprints:
        app.register_blueprint(blueprint, url_prefix=url_prefix)


def configure_extensions(app, extensions):

    for extension in extensions:
        extension.init_app(app)


def configure_security(app, security, user_datastore):

    security.init_app(app, user_datastore)


def configure_apis(app, apimgr, db, models):

    apimgr.init_app(app, flask_sqlalchemy_db=db)

    for model, methods, prefix in models:
        if not type(methods) == list:
            methods = [ methods ]
        # Seems like the 'app' parameter shouldn't be required here, but then
        # again it actually is, because init_app() doesn't set self.app for
        # the instantiated object. This might be a bug in Flask-Restless.
        # ref.: https://tinyurl.com/mq6fnul (flask_restless/manager.py#L381)
        apimgr.create_api(model, app=app, methods=methods, url_prefix=prefix)
