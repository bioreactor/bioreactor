from datetime import datetime as dt
from humanize import naturaltime, naturaldate

from ..utils import git_link_for

__all__ = [ 'humantime', 'humandate', 'gitlinkfor' ]


def humantime(dt):
    """
    Format a datetime date in a human-friendly way
    """
    #return naturaltime(dt.strptime(s, '%Y-%m-%dT%H:%M:%S.%f'))
    return naturaltime(dt)


def humandate(dt):
    return naturaldate(dt)


def gitlinkfor(cid):
    """
    Return a link to the Git 'origin' repo at the commit id 'cid'
    """
    return git_link_for(cid)

