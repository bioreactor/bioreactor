from flask import Markup, current_app, render_template, url_for, g

from ..utils import get_git_info, git_link_for

__all__ = [ 'environment', 'is_dev_server', 'release' ]


def environment():
    """
    Return the type of environment the application is running in
    
    e.g., 'prod', 'dev', 'testing') so that templates can use it, say to show
    extra debugging information
    """
    return current_app.config['ENVIRON']


def is_dev_server():
    """
    Return whether the application is running in the 'dev' environment
    """
    return current_app.config['ENVIRON'].startswith('dev')


def release():
    """
    Return an HTML snippet indicating some properties about the app
    
    This includes: the application name, checked-out branch and Git commit ID
    """
    if hasattr(g, 'release'):
        return g.release

    # otherwise...
    (branch, commit) = get_git_info()
    shortid = commit[0:7]
    branchurl = git_link_for(branch)
    commiturl = git_link_for(commit)

    g.release = render_template(
        'partials/app_info.html', branchurl=branchurl, branch=branch,
        shortid=shortid, commiturl=commiturl
    )
            
    return Markup(g.release)
