import os
import re
from git import Repo
from flask import g, current_app

__SHA1_RE     = re.compile('[a-f0-9]{40}')
__LEGAL_CHARS = '-_a-zA-Z0-9!%()' # FIXME: probably not permissive enough
__MAX_CHARS   = 32
__FILENAME_RE = re.compile(r'^[%s]{1,%i}$' % (__LEGAL_CHARS, __MAX_CHARS))


def is_legal_filename(string):
    """
    Check to see if the given string looks okay for a filename
    """
    return not __FILENAME_RE.match(string) is None


def make_legal_filename(string):
    """
    Strip illegal characters from string and return the result
    """
    string = string.strip().replace(' ', '_')
    string = re.sub(r'[^%s]' % __LEGAL_CHARS, '', string)
    # Clean up any dangling "spaces" that were substituted above
    string = re.sub(r'^_+|_+$', '', string)
    return string


def get_git_info():
    """
    Return the checked-out branch and commit ID of the git repo running the app
    """

    if not hasattr(g, 'gitrepo'):
        g.gitrepo = Repo(os.path.join(current_app.root_path, '..'))
        g.gitbranch = g.gitrepo.head.ref.__str__()
        g.gitcommit = g.gitrepo.head.commit.__str__()

    return (g.gitbranch, g.gitcommit)


def git_link_for(ref_or_cid):
    """
    Return a link to the 'origin' repo at the commit ID or ref
    
    :param string ref_or_cid: the commit ID/branch/ref to generate a link for
    """
    if not 'GIT_ORIGIN' in current_app.config:
        return False

    if __SHA1_RE.match(ref_or_cid):
        return current_app.config['GIT_ORIGIN'] + '/commit/' + ref_or_cid
    else:
        return current_app.config['GIT_ORIGIN'] + '/branches/' + ref_or_cid


def enumerate_files_in(*dirs):
    """
    Enumerate a list of files in the directories given by *dirs

    If the given directories don't have a leading slash, then prepend
    current_app.root_path.

    Source: https://stackoverflow.com/a/9511655
    """
    extra_dirs = []
    extra_files = []

    for dir in dirs:
        if not dir.startswith(os.sep):
            dir = os.path.join(current_app.root_path, dir)
        extra_dirs.append(dir)

    # If you wanted to also return the directories themselves; for our
    # purposes (giving Werkzeug a list of templates to monitor), we don't want
    # the directories as part of the list
    #extra_files = extra_dirs[:]

    for extra_dir in extra_dirs:
        for dirname, dirs, files in os.walk(extra_dir):
            for filename in files:
                if '.swp' in filename:
                    pass
                filename = os.path.join(dirname, filename)
                if os.path.isfile(filename):
                    extra_files.append(filename)

    return extra_files
