"""
Job Model for Bioreactor
"""
from datetime import datetime as dt
from uuid import uuid4

from bioreactor.extensions import db
from .user import User

def id_generator(size=10):
    """
    Return a hex job ID (default 10 digits)

    >>> id_generator(9)
    Traceback (most recent call last):
    ...
    ValueError: size must be between 10 and 40
    >>> id_generator(41)
    Traceback (most recent call last):
    ...
    ValueError: size must be between 10 and 40
    >>> len(id_generator(14)) == 14
    True
    """
    if size < 10 or size > 40:
        raise ValueError('size must be between 10 and 40')

    return uuid4().get_hex()[0:size]


def populate_jobs():
    """
    Fill the 'jobs' table with a bunch of random jobs
    """
    for x in range(1,11):
        db.session.add(Job(name='Test job #%s' % x, user_id=x,
                           description='A longer description of the test job',
                           notification_email='nobody@example.org'))
    db.session.commit()


class Job(db.Model):
    """A cluster job and some details about it"""
    id = db.Column(db.Integer, primary_key=True)
    jobid = db.Column(db.String(40), unique=False, nullable=False)
    uuid = db.Column(db.String(40), unique=True, nullable=False)
    name = db.Column(db.String(80))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    submitted_by = db.relationship('User', backref=db.backref('jobs',
            lazy='dynamic'))
    notification_email = db.Column(db.String(256))
    description = db.Column(db.Text)
    created_at = db.Column(db.DateTime(timezone=True), nullable=False)
    parameters = db.Column(db.Text)

    def __init__(self, name=None, user_id=None, description=None,
                notification_email=None):
        self.jobid = id_generator()
        self.uuid = uuid4().get_hex()
        self.name = name
        self.user_id = user_id
        self.description = description
        self.notification_email = notification_email
        self.created_at = dt.now()
        self.parameters = '{}'

    def __repr__(self):
        return '<Job: %s, %s>' % (self.jobid, self.name)

    def __str__(self):
        return self.name
