"""
Bioreactor Organism model (for demo API)
"""
from flask import current_app

from bioreactor.extensions import db


def populate_organisms():
    # populate the 'organism' table
    with open(current_app.config['DB_ORGANISM_CSV'], 'r') as organisms:
        for organism in organisms.readlines():
            db.session.add(Organism(name=organism.replace('\n', '')))
    db.session.commit()


class Organism(db.Model):
    """A table of species for which experimental data is available"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)

    # Courtesy: https://stackoverflow.com/a/11884806
    def as_dict(self):
        """
        Return the entire table as a dictionary

        (Note that you can also access the internal dictionary of SQLAlchemy
        objects with '.__dict__'.)
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Organism: %s, %s>' % (self.id, self.name)

    def __str__(self):
        return self.name
