"""
Bioreactor Role model (for Flask-Security roles)
"""
from flask_security import RoleMixin

from bioreactor.extensions import db


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), unique=True, nullable=False)
    description = db.Column(db.String(80))

    def __repr__(self):
        return '<Role: %s, %s>' % (self.id, self.name)

    def __str__(self):
        return self.name


roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

