"""
Bioreactor User model
"""
from datetime import datetime as dt

from flask_security import UserMixin

from bioreactor.extensions import db
from .role import Role, roles_users


def populate_users():
    # FIXME
    pass


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(16), unique=True, nullable=False)
    password = db.Column(db.String(256))
    email = db.Column(db.String(256), unique=True)
    active = db.Column(db.Boolean, nullable=False)
    created_at = db.Column(db.DateTime(timezone=True), nullable=False)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
            backref=db.backref('users', lazy='dynamic'))

    def __init__(self, username, password=None, email=None, active=True,
                 roles=None):
        self.username = username
        self.password = password
        self.email = email
        self.active = active
        self.created_at = dt.now()
        self.confirmed_at = None
        self.roles = roles

    def __repr__(self):
        return '<User: %s, %s>' % (self.id, self.username)

    def __str__(self):
        return self.username
