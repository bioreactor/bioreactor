IMAGE SOURCES
=============

chemical.png  https://openclipart.org/detail/203314/science
beaker.png    https://openclipart.org/detail/172920/beaker

Made available under CC0 licenses; see https://openclipart.org/share.
