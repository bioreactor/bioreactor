function toggleBranch(e) {
  if (e.parent().hasClass('collapsed')) {
    e.parent().removeClass('collapsed');
  } else {
    e.parent().addClass('collapsed');
  }
}

$(function() {
  $('.qty').each(function(i) {
    $(this).click(function() {
        toggleBranch($(this));
    });
  });
});
