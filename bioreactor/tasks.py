import os
from flask import current_app

from bioreactor.factory import create_celery_app

taskq = create_celery_app()


class BioreactorTaskQueueUnavailable(Exception):
    pass


def get_celery_worker_status():
    """
    Check status of the Celery task queue
    Source: https://stackoverflow.com/a/8522470
    """
    try:
        from celery.task.control import inspect
        insp = inspect()
        d = insp.stats()
        if not d:
            raise BioreactorTaskQueueUnavailable('No celery worker found')
    except IOError as e:
        from errno import errorcode
        msg = "Error connecting to the backend: " + str(e)
        if len(e.args) > 0 and errorcode.get(e.args[0]) == 'ECONNREFUSED':
            msg += ' Check that the RabbitMQ server is running.'
        raise BioreactorTaskQueueUnavailable(msg)
    except ImportError as e:
        raise BioreactorTaskQueueUnavailable(str(e))
    return True


@taskq.task
def add(x, y):
    current_app.logger.debug('Sleeping for 15 seconds...')
    os.system('sleep 15')
    current_app.logger.debug('Done sleeping.')
    return x + y


@taskq.task
def send_mail(subject, recipient, reply_to, body):
    """
    Send an email asynchronously with the task queue
    """
    from flask_mail import Message
    from bioreactor.extensions import mail

    msg = Message(subject=subject, recipients=[recipient], reply_to=reply_to,
                  body=body)

    current_app.logger.debug('Sleeping for 15 seconds...')
    os.system('sleep 15')
    current_app.logger.debug('Done sleeping.')

    try:
        import re
        sanipass = re.sub(r'(.).*(.)$', r'\1xxxxx\2',
                          current_app.config.get('MAIL_PASSWORD', ''))
        user = current_app.config.get('MAIL_USERNAME', '<empty>'),

        if current_app.config.get('DEBUG', ''):
            current_app.logger.debug(
                'Sending mail as {} (pass: {}) to {}'.format(
                    user, sanipass, recipient))

        mail.send(msg)

    except Exception as e:
        current_app.logger.error('Error sending email "%s" to %s'
                                 % (subject, recipient))
        current_app.logger.error('Exception: %s' % e)

    else:
        current_app.logger.info('Successfully sent email "%s" to %s'
                                 % (subject, recipient))


#class TaskQueue(Celery):
#    """
#    Celery task queue; adds an 'init_app' method which subclasses
#    celery.app.task and wraps it in a Flask application context
#    """
#    # Just could *not* get this to work. The getters for 'backend' and
#    # 'broker' were somehow broken on the subclass instance, and just returned
#    # strings instead of instances of some object. :( *tears*
#    #def __init__(self):
#    #    """
#    #    Configure the Celery instance from a Flask app config
#    #    """
#    #    from bioreactor.config import Configuration as cfg
#    #    super(TaskQueue, self).__init__('tasks', backend=cfg.CELERY_BROKER_URL
#    #                                             broker=cfg.CELERY_RESULT_BACKEND)
#
#    # Source: http://flask.pocoo.org/docs/0.12/patterns/celery/
#    def init_app(self, app):
#        TaskBase = self.Task
#
#        class ContextTask(TaskBase):
#            abstract = True
#            def __call__(self, *args, **kwargs):
#                with app.app_context():
#                    return TaskBase.__call__(self, *args, **kwargs)
#
#        self.Task = ContextTask
