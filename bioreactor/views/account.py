import os
from flask import Blueprint, current_app, session, request, flash, \
                  render_template, redirect, url_for

from bioreactor.extensions import db
from bioreactor.models import User

account = Blueprint('account', __name__)


@account.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != current_app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != current_app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('frontend.home'))

    return render_template(
        'login.html', error=error,
        default_username=current_app.config['USERNAME'],
        default_password=current_app.config['PASSWORD']
    )


@account.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('frontend.home'))
