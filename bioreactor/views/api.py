from flask import Blueprint, current_app, jsonify, redirect, request, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text

from bioreactor.extensions import db
from bioreactor.models import Organism

api = Blueprint('api', __name__)


@api.route('/')
def index():
    """Redirect casual users to the API help page"""
    return redirect(url_for('api.help'))


@api.route('/help', methods = ['GET'])
def help():
    """Print available functions."""
    func_list = {}
    for rule in current_app.url_map.iter_rules():
        if not rule.rule.startswith('/api'):
            continue
        func_list[rule.rule] = \
            current_app.view_functions[rule.endpoint].__doc__.split('\n')[0]
    return jsonify(func_list)


@api.route('/organism/search')
def search_organisms():
    """Substring match using 'q=' query string parameter"""
    # Return anything with a substring match on the 'name' field
    cond = Organism.name.like("%{}%".format(request.args.get('q', '')))
    return jsonify([match.as_dict() for match in Organism.query.filter(cond)])


@api.route('/organism/autocomplete')
def autocomplete_organisms():
    """Search results for autocomplete control"""
    cond = Organism.name.like("%{}%".format(request.args.get('q', '')))
    matches = [match.as_dict() for match in Organism.query.filter(cond)]

    # Rename the 'name' field to 'text' to make select2.js happy
    return jsonify(map(lambda x: {'id': x['id'], 'text': x['name']}, matches))
