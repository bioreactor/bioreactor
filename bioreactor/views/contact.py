from flask import Blueprint, flash, request, redirect, render_template, \
                  current_app, url_for, make_response, g

contact = Blueprint('contact', __name__)

@contact.before_request
def check_celery():
    from bioreactor.tasks import BioreactorTaskQueueUnavailable, \
                                 get_celery_worker_status
    try:
        g.celery_unavailable = False;  # this is sketchy
        get_celery_worker_status()
    except BioreactorTaskQueueUnavailable:
        g.celery_unavailable = True;
        # FIXME: The Boostrap style isn't getting picked up here.
        flash('Background task processor unavailable!', category='error')


@contact.route('/')
def form():
    return render_template('contact.html')


@contact.route('/post', methods=['POST'])
def post():
    from bioreactor.extensions import mail
    from bioreactor.tasks import add, send_mail

    # FIXME: use validate_email to check this out. Also, what if they're blank?
    sender = '"{}" <{}>'.format(request.form['name'],
                                request.form['email'])
    subject = current_app.config['APP_NAME'] + ' contact form'
    recipient = current_app.config['ADMIN_EMAIL']
    body = request.form['message']

    if g.celery_unavailable:
        send_mail(subject=subject, recipient=recipient, reply_to=sender,
                  body=body)
        flash('Your message was sent. Thanks!')
    else:
        send_mail.delay(subject=subject, recipient=recipient, reply_to=sender,
                        body=body)
        flash('Your message was sent (deferred). Thanks!')

    return redirect(url_for('.form'))
