from sqlalchemy import desc
from flask import Blueprint, current_app, session, request, render_template, \
                  redirect

from bioreactor.models import Job

# because current_app.config is not available outside the application context
frontend = Blueprint('frontend', __name__)


@frontend.route('/')
def home():
    joblist = []
    firstlaunch_cookie = 0
    if 'bioreactor_firstlaunch' in request.cookies:
        firstlaunch_cookie = request.cookies['bioreactor_firstlaunch']
    try:
        joblist = Job.query.order_by(desc(Job.created_at)).limit(5).all()
    finally:
        return render_template('home.html', job_list=joblist, narrow=True,
                               firstlaunch_cookie=firstlaunch_cookie)


@frontend.route('/about')
def about():
    return render_template('about.html')


@frontend.route('/search')
def search_results():
    return render_template('search_results.html')
