import os
from sqlalchemy import desc
from flask import Blueprint, session, request, flash, render_template, \
                  redirect, url_for, abort

from bioreactor.extensions import db
from bioreactor.models import Organism, Job

jobs = Blueprint('jobs', __name__)


@jobs.route('/jobs/')
def job_list():
    joblist = []
    try:
        joblist = Job.query.order_by(desc(Job.created_at)).all()
    finally:
        return render_template('jobs.html', job_list=joblist)


@jobs.route('/jobs/<id>')
def results_for_job_id(id):
    # FIXME: This doesn't actually return any "results," just the same
    # information about the job that you see in the job list
    job_result=[]
    try:
        job_result = Job.query.filter(Job.id == id).first()
    finally:
        return render_template('job_result.html', job_result=job_result)


@jobs.route('/jobs/add', methods=['GET'])
def add_job():
    return render_template('add_job.html')

@jobs.route('/jobs/add', methods=['POST'])
def add_job_post():
    if not session.get('logged_in'):
        abort(401)

    job = Job(name=request.form['name'],
              description=request.form['description'],
              notification_email=request.form['email'])
    db.session.add(job)
    db.session.commit()
    flash('New job was successfully submitted')
    return redirect(url_for('jobs.job_list'))
