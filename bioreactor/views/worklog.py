import os
from markdown import Markdown
from bleach import linkify
from flask import Blueprint, Markup, render_template, current_app

worklog = Blueprint('worklog', __name__)


@worklog.route('/')
def worklog():
    """
    View function for the work log
    """
    mdfile = os.path.join(current_app.config['PROJECT_ROOT'], 'WORKLOG.md')
    with current_app.open_resource(mdfile, 'r') as f:
        sourcetext = unicode(f.read(), 'utf-8')

    # Borrowed from https://github.com/skurfer/RenderMarkdown
    md_ext = ['extra', 'codehilite']

    md = Markdown(extensions=md_ext, output_format='html5')

    # If you needed it, here's how to force a string into UTF-8 format.
    #     mdown = mdown.decode('utf8', 'ignore')
    # ...thanks, https://stackoverflow.com/a/20768800
    mdown = Markup(linkify(md.convert(sourcetext), skip_pre=True))

    return render_template('work_log.html', worklog=mdown)
