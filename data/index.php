<?php 
# 
# Implement the 'login()' method for Adminer so that loading SQLite databases 
# directly from the filesystem is possible (otherwise you get a warning from
# Adminer).
#
# Source:
# https://github.com/vrana/adminer/commit/7a33661b721714a8b266bf57c0065ae653bb8097
#
# NB: It would be possible to require a username / password combination here,
# but a better / more obvious method would be to add HTTP authentication to a
# <Directory> section in the Apache configuration.
#
# Here's what that might look like:
#
#   <Directory /var/www/bioreactor/db>
#       Options -Indexes
#
#       # Ref: https://httpd.apache.org/docs/2.4/howto/auth.html
#       AuthType Basic
#       AuthName "Bioreactor database admin"
#       # Create a username/password pair with 'htpasswd -c <dbname> <user>'
#       AuthUserFile "/var/www/bioreactor/htpasswd"
#
#       Order deny,allow
#       Allow from all
#       Require valid-user
#   </Directory>
#   Alias "/dbadmin" "/var/www/bioreactor/db"
#
#

define('DEFAULT_DB', 'bioreactor.db');

# Ref: https://www.adminer.org/en/extension/
function adminer_object() {
    class AdminerSoftware extends Adminer {
        function login($login, $password) {
            return true;
        }

        function loginForm() {
            global $drivers;
            $db = array_key_exists('db', $_GET) ? h($_GET['db']) : DEFAULT_DB;
            echo <<<"HTML"
<table cellspacing="0">
  <tr>
    <th>System</th>
    <td><input name="auth[driver]" type="hidden" value="sqlite"><code>sqlite</code></td>
  <tr>
    <th>Database file</th>
    <td><input id='database' name="auth[db]" value="$db" autocapitalize="off" autofocus="true"></td>
</table>
<p><input type="submit" value="Open SQLite DB"></p>
HTML;
        } // loginForm
    } // class AdminerSoftware
    return new AdminerSoftware;
} // adminer_object

include "./adminer-4.2.5.php";
