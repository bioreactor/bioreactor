#!/bin/sh
export FLASK_APP=bioreactor/app.py
export FLASK_DEBUG=1

cd /deploy
source venv/bin/activate
flask run --host=0.0.0.0

# couple of alternatives using Supervisor
#supervisord -c /deploy/supervisord.conf
#supervisorctl start bioreactor:frontend
