from setuptools import setup

with open('requirements.txt', 'r') as f:
    requires=[line for line in f.read().split("\n")
              if line and not line.startswith('#')]

setup(
    name='bioreactor',
    version='0.0.1',
    packages=[
        'bioreactor',
    ],
    package_data={
        'bioreactor': ['static/*', 'templates/*'],
    },
    install_requires=requires,
    entry_points='''
        [console_scripts]
        bioreactor=bioreactor.cli:cli
        # Also interesting:
        # [flask.command]
    ''',
)
